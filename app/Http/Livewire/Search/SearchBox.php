<?php

namespace App\Http\Livewire\Search;

use Livewire\Component;

class SearchBox extends Component
{

    public function render()
    {
        return view('livewire.search.search-box');
    }
}
