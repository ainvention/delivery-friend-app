<?php

namespace App\Http\Livewire\Search;

use Livewire\Component;

class Step2 extends Component
{
    public function render()
    {
        return view('livewire.search.step2');
    }
}
